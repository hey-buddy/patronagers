from django.db import models

from base.models import BaseModel


class Imagem(BaseModel):
    """
    Base image class to be used on other models.
    """

    path = models.ImageField()
    descricao = models.CharField(max_length=255)

    def __str__(self):
        return f"{self.path}\n{self.descricao}"


class Pessoa(BaseModel):
    """
    Classe base de pessoa a ser herdada.
    """

    nome = models.CharField(max_length=80)
    data_nascimento = models.DateField()
    email = models.EmailField(max_length=75, blank=True)
    telefone = models.CharField(max_length=12, blank=True)
    imagens = models.ManyToManyField(Imagem, through="PessoaImagem")

    @property
    def age(self):
        pass

    def __str__(self):
        return f"{self.nome}"


class PessoaImagem(BaseModel):
    """
    Representa relacao entre Pessoa e Imagem.
    """

    pessoa = models.ForeignKey("Pessoa", on_delete=models.PROTECT)
    imagem = models.ForeignKey("Imagem", on_delete=models.PROTECT)


class Patrono(Pessoa):
    """
    Pessoa/Empresa que irá doar/ajudar com algo.
    """

    pass


class Apatronado(Pessoa):
    """
    Pessoa/Ser precisando de ajuda.
    """

    descricao = models.TextField(blank=True)

    def __str__(self):
        return f"{self.nome}"


class UnidadeMedida(BaseModel):
    nome = models.CharField(max_length=50)
    sigla = models.CharField(max_length=5)

    def __str__(self):
        return f"{self.nome}"


class Item(BaseModel):
    nome = models.CharField(max_length=50)
    descricao = models.TextField(blank=True)
    imagem = models.ImageField(blank=True)
    unidade_medida = models.ForeignKey("UnidadeMedida", on_delete=models.PROTECT)

    def __str__(self):
        return f"{self.nome}"


class Evento(BaseModel):
    """
    Agrega um evento a uma colecao de Doacoes.
    """

    nome = models.CharField(max_length=50)
    descricao = models.TextField()
    imagens = models.ManyToManyField(Imagem, through="EventoImagem")

    def __str__(self):
        return f"{self.nome}\n{self.descricao}"


class EventoImagem(BaseModel):
    image = models.ForeignKey(
        "Imagem", related_name="eventos", on_delete=models.PROTECT
    )
    evento = models.ForeignKey("Evento", on_delete=models.PROTECT)


class Doacao(BaseModel):
    """
    Doacao: item ou quantidade de items a serem doados de um patrono para um
    apatronado.
    """

    patrono = models.ForeignKey(
        "Patrono", related_name="doacoes", on_delete=models.PROTECT
    )
    apatronado = models.ForeignKey(
        "Apatronado", related_name="doacoes", on_delete=models.PROTECT
    )
    items = models.ManyToManyField(Item, through="ItemDoacao")
    evento = models.ForeignKey(
        "Evento", related_name="doacoes", on_delete=models.PROTECT, blank=True
    )

    def __str__(self):
        return f"{self.nome}"


class ItemDoacao(BaseModel):
    """
    Representa relacao entre Item e Doacao.
    """

    quantidade = models.FloatField()
    doacao = models.ForeignKey("Doacao", on_delete=models.PROTECT)
    item = models.ForeignKey("Item", on_delete=models.PROTECT)
