# Generated by Django 3.0.5 on 2020-06-02 04:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Doacao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('nome', models.CharField(max_length=50)),
                ('descricao', models.TextField(blank=True)),
                ('imagem', models.ImageField(blank=True, upload_to='')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Pessoa',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('nome', models.CharField(max_length=80)),
                ('data_nascimento', models.DateField()),
                ('email', models.EmailField(blank=True, max_length=75)),
                ('telefone', models.CharField(blank=True, max_length=12)),
                ('habilitado', models.BooleanField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='UnidadeMedida',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('nome', models.CharField(max_length=50)),
                ('sigla', models.CharField(max_length=5)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Administrador',
            fields=[
                ('pessoa_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='patronagem.Pessoa')),
            ],
            options={
                'abstract': False,
            },
            bases=('patronagem.pessoa',),
        ),
        migrations.CreateModel(
            name='Apatronado',
            fields=[
                ('pessoa_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='patronagem.Pessoa')),
                ('descricao', models.TextField(blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('patronagem.pessoa',),
        ),
        migrations.CreateModel(
            name='Patrono',
            fields=[
                ('pessoa_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='patronagem.Pessoa')),
            ],
            options={
                'abstract': False,
            },
            bases=('patronagem.pessoa',),
        ),
        migrations.CreateModel(
            name='ItemDoacao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('quantidade', models.FloatField()),
                ('doacoes', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='patronagem.Doacao')),
                ('items', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='patronagem.Item')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='item',
            name='unidade_medida',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='patronagem.UnidadeMedida'),
        ),
        migrations.AddField(
            model_name='doacao',
            name='items',
            field=models.ManyToManyField(through='patronagem.ItemDoacao', to='patronagem.Item'),
        ),
        migrations.AddField(
            model_name='doacao',
            name='apatronado',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='patronagem.Apatronado'),
        ),
        migrations.AddField(
            model_name='doacao',
            name='patrono',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='patronagem.Patrono'),
        ),
    ]
