from django.contrib import admin
from .models import Patrono, Apatronado, UnidadeMedida, Item, Doacao, ItemDoacao

models = [Patrono, Apatronado, UnidadeMedida, Item, Doacao, ItemDoacao]

admin.site.register(models)
